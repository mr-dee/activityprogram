package framework;

/**
 * GenericItem.java
 * 
 * A class to construct an object of type GenericItem and includes methods to
 * manipulate a GenericItem object.
 * 
 * @author George Dee
 *
 */
public class GenericItem {
	
	private String content;
	
	/**
	 * Constructor to create GenericItem object
	 */
	public GenericItem(){}
	
	/**
	 * Constructor to create GenericItem object
	 * 
	 * @param content	content to be put into object
	 */
	public GenericItem(String content){
		this.content = content;
	}
	
	/**
	 * Get the content of the GenericItem object
	 * 
	 * @return	content of the GenericItem object
	 */
	public String getContent(){
		return content;
	}
	
	/**
	 * Set the content of the GenericItem object
	 * 
	 * @param content	content to be put into object
	 */
	public void setContent(String content){
		this.content = content;
	}

}
