package framework;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.app.ListFragment;
import android.os.Bundle;

import com.uea.activityprogram.R;

import framework.GenericItem;
import framework.GenericListAdapter;
import framework.RemoteDataSource;

/**
 * GenericXmlParser.java
 * 
 * A class to retrieve XML document and print out the contents of it using a
 * ListAdapter.
 * 
 * @author George Dee
 *
 */
public class GenericXmlParser extends ListFragment {
	
	//Used to hold list of GenericItems
	private ArrayList<GenericItem> genericItemList;
	//Used to retrieve child node
	private Node objects;
	//Used to retrieve children
	private NodeList children;

	// Default XML displayed if no custom tab set
	public String url = "https://dl.dropboxusercontent.com/u/91815775/NoCustomFeed.xml";

	/**
	 * Constructor to create empty GenericList object
	 */
	public GenericXmlParser() {
	}

	/**
	 * Constructor to create GenericList object
	 * 
	 * @param url
	 *            url to be set to GenericList object
	 */
	public GenericXmlParser(String url) {
		this.url = url;
	}
	
	/**
	 * Method called on instantiation which retrieves XML document and prints 
	 * the contents from it using ListAdapter
	 * 
	 * @param savedInstanceState	previously saved state
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//ArrayList to hold GenericItem objects
		genericItemList = new ArrayList<GenericItem>();

		//Use RemoteDataSource to get Document of given URL
		RemoteDataSource rds = RemoteDataSource.getInstance();
		Document xmlFile = rds.getResource(url);
		
		//Retrieve child node of document element
		objects = xmlFile.getDocumentElement();
		
		//Produce NodeList of children from document element
		children = objects.getChildNodes();
		
		//Iterate through node list, extract content from each node, set in a
		//GenericItem object and store in GenericItem ArrayList
		if (children != null && children.getLength() > 0) {
			for (int i = 0; i < children.getLength(); i++) {
				if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
					Element e = (Element) children.item(i);
					GenericItem genericItem = new GenericItem();
					String nodeValue = e.getTextContent();
					genericItem.setContent(nodeValue);
					genericItemList.add(genericItem);
				}
			}
		}

		/**
		 * The following code has same purpose of the code above but kept for
		 * possible extensibility
		 * 
		for (Node object = objects.getFirstChild(); object != null; object = object
				.getNextSibling()) {
			if (object instanceof Element) {
				Element e = (Element) object;
				GenericItem genericItem = new GenericItem();
				String nodeValue = e.getTextContent();
				genericItem.setContent(nodeValue);
				genericItemList.add(genericItem);
			}
		}
		**/

		//Add genericItemList to ListView
		GenericListAdapter genericItemAdapter = new GenericListAdapter(
				getActivity(), genericItemList, R.layout.custom_feed_view);
		
		//Set listAdapter of ListFragment to display list
		setListAdapter(genericItemAdapter);

	}
	
	/**
	 * Method to get element from a given node
	 * 
	 * @param elem	element to retrieve value from
	 * @return	String containing node value
	 */
	public final String getElementValue(Node elem) {
		Node child;
		if (elem != null) {
			if (elem.hasChildNodes()) {
				for (child = elem.getFirstChild(); child != null; child = child
						.getNextSibling()) {
					if (child.getNodeType() == Node.TEXT_NODE) {
						return child.getNodeValue();
					}
				}
			}
		}
		return "";
	}
	
	/**
	 * Method to get value from an element
	 * 
	 * @param item	element to retrieve value from
	 * @param str	name of element to find
	 * @return	method call to getElementValue
	 */
	public String getValue(Element item, String str) {
		NodeList n = item.getElementsByTagName(str);
		return this.getElementValue(n.item(0));
	}
	
	/**
	 * Method to set URL of GenericXmlParser object
	 * 
	 * @param url	URL to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

}
