package framework;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.uea.activityprogram.R;
/**
 * GenericListAdapter.java
 * 
 * A class to construct an Adapter which can be used to display GenericItem 
 * objects in an ArrayList within the application.
 * 
 * @author George Dee
 *
 */
public class GenericListAdapter extends ArrayAdapter<GenericItem>{
	
	private final Context context;
	private final ArrayList<GenericItem> genericItemList;
	private final int layoutResourceId;
	
	/**
	 * Constructor to create a GenericListAdapter object
	 * 
	 * @param context			context of Activity
	 * @param busList			ArrayList of GenericItem objects
	 * @param layoutResourceId	int containing id of layout resource
	 */
	public GenericListAdapter(Context context, ArrayList<GenericItem> genericItemList, 
			int layoutResourceId){
		super(context, layoutResourceId, genericItemList);
		this.context = context;
		this.genericItemList = genericItemList;
		this.layoutResourceId = layoutResourceId;
	}
	
	/**
	 * Get view to be displayed on application which shows content from 
	 * genericItemList
	 * 
	 * @param position		int containing position
	 * @param convertView	convert view
	 * @param parent		parent ViewGroup
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View row = convertView;
		ViewHolder holder = null;
		
		if(row == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(layoutResourceId, parent, false);
			
			holder = new ViewHolder();
			holder.content = (TextView)row.findViewById(R.id.content);
			
			row.setTag(holder);
		} else {
			holder = (ViewHolder)row.getTag();
		}
		
		GenericItem genericItem = genericItemList.get(position);
		
		holder.content.setText(genericItem.getContent());
		
		return row;
	}
	
	/**
	 * Class to hold TextView variables for each attribute in GenericItem 
	 * object
	 * 
	 * @author George Dee
	 *
	 */
	static class ViewHolder
    {
        TextView content;
    }

}
