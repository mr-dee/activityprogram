package framework;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ExecutionException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.os.AsyncTask;
import android.util.Log;

public class RemoteDataSource {
	
	//Stores a reference to itself to implement the singleton design pattern
	private static RemoteDataSource instance = null;

	//The default URL where this RemoteDataSource will go to first
	private String baseUrl;
	//A Queue of String URL requests from apps
	private Queue resourceQueue;
	//A list of Documents requested and returned for apps
	private ArrayList<Document> files;
	//An array containing the most recently requested resources
	private String currentUrl[];
	//A HTTP request that is used to connect to remote data sources
	private DefaultHttpClient httpClient;
	//A poster that posts a request to a resource though a DefaultHttpRequest
	private HttpPost httpPoster;
	//Returns the http request from a remote source
	private HttpResponse httpResponse;
	//The Content of the response of the request
	private HttpEntity responseContent;

	//The factory that creates concrete Document objects
	private DocumentBuilderFactory documentFactory;
	private DocumentBuilder documentBuilder;

	//The current xml file that is being received
	private Document currentXml;



	/**
	 * Default constructor. Constructs a new RemoteDataSource Singleton
	 * with a baseUrl of "https://www.uea.ac.uk/~fqv12suu/default.rss"
	 */
	protected RemoteDataSource(){
		try{
			this.baseUrl = "https://dl.dropboxusercontent.com/u/91815775/ActivityFinderData.xml";
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Gets current instantiation of the RemoteDataSource
	 * @return			the current RemoteDataSourceSingleton
	 */
	public static RemoteDataSource getInstance(){
		if(instance == null){
			instance = new RemoteDataSource();
		}
		return instance;
	}

	/**
	 * Method to execute AsyncTask
	 *
	 * @param url			A String representing the URL of the data source
	 * @return				A Document wrapping the XML file.
	 */
	public Document getResource(String url){
		try {
			currentXml = new DownloadXmlDoc().execute(url).get();
		} catch (InterruptedException e) {
			Log.d("RDS getResource Error: ", e.getMessage());
			e.printStackTrace();
			return null;
		} catch (ExecutionException e) {
			Log.d("RDS getResource Error: ", e.getMessage());
			e.printStackTrace();
			return null;
		}
		return currentXml;
	}
	
	/**
	 * Method to return Document store in currentXml
	 * 
	 * @return	Document containing XML
	 */
	public Document getCurrentXML(){
		return this.currentXml;
	}
	
	 private class DownloadXmlDoc extends AsyncTask<String, Void, Document>{
		 
		/**
		 * Takes a single URL, retrieves the XML file from that location,
		 * wraps it up in a Document and returns it. Also sets currentUrl
		 * and currentXml in the RemoteDataSource
		 * 
		 * @param url	A String representing the URL of the data source 
		 */
		@Override
		protected Document doInBackground(String... url) {
			try{
				if (documentFactory == null) {
					documentFactory = DocumentBuilderFactory.newInstance();
				}
				if(httpClient == null){
					httpClient = new DefaultHttpClient();
				}
			//Get the XML file from remote
			httpPoster = new HttpPost(url[0]);
			httpResponse = httpClient.execute(httpPoster);
			responseContent = httpResponse.getEntity();
			String currentString = null;
			currentString = EntityUtils.toString(responseContent);

			//Wrap it in a Document
			Document progressXml = null;
			documentBuilder = documentFactory.newDocumentBuilder();
			InputSource input = new InputSource();
			input.setCharacterStream(new StringReader(currentString));
			progressXml = documentBuilder.parse(input);
			return progressXml;
			}
				//Catches exceptions from the DocumentBuilder instansiation
				catch(ParserConfigurationException e){
					Log.d("RDS getResource Error: ", e.getMessage());
					e.printStackTrace();
					return null;
				}
				//IO and SAX exceptions from creating the Document and Parsing
				catch(SAXException e){
					Log.d("RDS getResource Error: ", e.getMessage());
					e.printStackTrace();
					return null;
				}
				catch(IOException e){
					Log.d("RDS getResource Error", e.getMessage());
					e.printStackTrace();
					return null;
				}
			
		}
		
	 }
	 
	 /**
	  * Refresh all current URL's and return ArrayList containing refreshed
	  * documents
	  * 
	  * @return	ArrayList with refreshed documents
	  */
	 public ArrayList refresh(){
		 for(int i=0; i<currentUrl.length; i++){
			 files.add(i, instance.getResource(currentUrl[i]));
		 }
		 return files;
	 }
	 
	 /**
	  * Retrieve cached file from ArrayList
	  * 
	  * @param pos	position of file
	  * @return	specified Document
	  */
	 public Document getById(int pos){
		 return files.get(pos);
	 }
	 
	 /**
	  * Set RemoteDataSource to receive single or multiple URL's on its next 
	  * refresh
	  * 
	  * @param urls	URL's to set
	  */
	 public void setMultipleResources(String[] urls){
		 for(int i=0; i<urls.length; i++){
			 currentUrl[i] = urls[i];
		 }
	 }

}