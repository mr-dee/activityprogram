package com.uea.activityprogram;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Spinner;

/**
 * MainActivity.java
 * 
 * Class first called when application is opened. Presents form to user to 
 * selected school and open day date.
 * 
 * @author George Dee
 *
 */
public class MainActivity extends Activity {
	
	/**
	 * Method called when class is created. Shows XML file.
	 *
	 * @param savedInstanceState	previously saved state
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.school_selection);
	}
	
	/**
	 * Method to add items to action bar if present
	 * 
	 * @param menu	used to display buttons
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/**
	 * Method to retrieve school and date values and pass correct URL to the
	 * ShowSchool class
	 * 
	 * @param view	view of list
	 */
	public void getSchool(View view){
		Intent intent = new Intent(MainActivity.this, ShowSchool.class);
    	
		//Set up spinners to retrieve values
		Spinner schoolSpinner = (Spinner) findViewById(R.id.school);
		Spinner dateSpinner = (Spinner) findViewById(R.id.date);
		
    	//Store field values as String
    	String xmlSchool = schoolSpinner.getSelectedItem().toString();
    	String xmlDate = dateSpinner.getSelectedItem().toString();
    	
    	String url = null;
    	
    	//Find correct XML file base on school and date values
    	if(xmlDate.contains("09/05/2015")){
    		if(xmlSchool.contains("CMP")){
    			url = "https://dl.dropboxusercontent.com/u/91815775/CMP09052015.xml";
    		} else if(xmlSchool.contains("NBS")){
    			url = "https://dl.dropboxusercontent.com/u/91815775/NBS09052015.xml";
    		} else if(xmlSchool.contains("HIS")){
    			url = "https://dl.dropboxusercontent.com/u/91815775/HIS09052015.xml";
    		} else if(xmlSchool.contains("BIO")){
    			url = "https://dl.dropboxusercontent.com/u/91815775/BIO09052015.xml";
    		} else {
    			url = "https://dl.dropboxusercontent.com/u/91815775/OpenDayUnavailable.xml";
    		}
    	} else if(xmlDate.contains("23/05/2015")){
    		if(xmlSchool.contains("CMP")){
    			url = "https://dl.dropboxusercontent.com/u/91815775/CMP23052015.xml";
    		} else if(xmlSchool.contains("NBS")){
    			url = "https://dl.dropboxusercontent.com/u/91815775/NBS23052015.xml";
    		} else if(xmlSchool.contains("HIS")){
    			url = "https://dl.dropboxusercontent.com/u/91815775/HIS23052015.xml";
    		} else if(xmlSchool.contains("BIO")){
    			url = "https://dl.dropboxusercontent.com/u/91815775/BIO23052015.xml";
    		} else {
    			url = "https://dl.dropboxusercontent.com/u/91815775/OpenDayUnavailable.xml";
    		}
    	}
    	
    	//Put URL value in intent
    	intent.putExtra("url", url);
    	//Send intent
    	startActivity(intent);
    	//Remove previous intent so the user must go through settings to change
    	//default school
    	finish();
		
	}

}
