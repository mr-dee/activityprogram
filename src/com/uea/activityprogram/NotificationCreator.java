package com.uea.activityprogram;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.uea.activityprogram.R;

public class NotificationCreator extends Activity{
	
	public NotificationCreator(){
		//Create notification
		NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
			.setSmallIcon(R.drawable.uea)
			.setContentTitle("Activity Reminder")
			.setContentText("Activity starting soon");
		
		//Intent when notification is pressed
		Intent resultIntent = new Intent(this, MainActivity.class);
		//Initialise stack builder 
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		stackBuilder.addParentStack(ShowSchool.class);
		stackBuilder.addNextIntent(resultIntent);
		//Create pending intent
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
		//Set content intent to notification
		notification.setContentIntent(resultPendingIntent);
		//Initialise notification manager and build notification
		NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(1, notification.build());
	}

}
