package com.uea.activityprogram;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.app.NotificationManager;
import framework.GenericXmlParser;

/**
 * ShowSchool.java
 * 
 * Class used to display XML file to user. It does this by displaying
 * ListFragment containing the XML in the Activity within this class.
 * 
 * @author George Dee
 *
 */
public class ShowSchool extends Activity{
	
	//Instance to parse document into ListFragment
	private GenericXmlParser parser;
	//URL chosen by the user
	private String chosenUrl = "";
	
	/**
	 * Method to display ListFragment containing XML data in a FrameLayout to
	 * be view within the Activity
	 * 
	 * @param savedInstanceState	previously saved state
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.data_display);
		
		//Initialise parser
		parser = new GenericXmlParser();
		
		//Retrieve intent and extra user chosen URL
		Intent in = getIntent();
		chosenUrl = in.getStringExtra("url");
		//If URL is not null, set as URL for custom tab
		if(chosenUrl != null){
			parser.setUrl(chosenUrl);
		}
		
		//Present ListFragment within FrameLayout
		getFragmentManager().beginTransaction().add(R.id.frag_container, parser).commit();

	}
	
	/**
	 * Method to add items to action bar if present
	 * 
	 * @param menu	used to display buttons
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/**
	 * Method to add behaviour to buttons when pressed
	 * 
	 * @param menu	menu item to be pressed
	 */
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			//Start new intent display feed choice
			startActivity(new Intent(ShowSchool.this, MainActivity.class));
			//Remove previous intent
			finish();
		} 
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Method to respond to a checked checkbox
	 * 
	 * @param view	view of list
	 */
	public void onCheckboxClicked(View view){
		//Check if box is checked
		boolean checked = ((CheckBox) view).isChecked();
		
		switch(view.getId()){
			case R.id.checkbox_activity:
				if(checked){
					//Create notification
					NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
						.setSmallIcon(R.drawable.uea)
						.setContentTitle("Activity Reminder")
						.setContentText("Activity starting soon");
					
					//Intent when notification is pressed
					Intent resultIntent = new Intent(this, MainActivity.class);
					//Initialise stack builder 
					TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
					stackBuilder.addParentStack(ShowSchool.class);
					stackBuilder.addNextIntent(resultIntent);
					//Create pending intent
					PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
					//Set content intent to notification
					notification.setContentIntent(resultPendingIntent);
					//Initialise notification manager and build notification
					NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
					mNotificationManager.notify(1, notification.build());
				}
		}
	}

}
