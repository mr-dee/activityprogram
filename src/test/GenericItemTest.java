package test;

import framework.GenericItem;

import junit.framework.TestCase;

public class GenericItemTest extends TestCase {

	public void testGetContent() {
		GenericItem tester = new GenericItem("content");
		assertEquals("Result", "content", tester.getContent());
	}

	public void testSetContent() {
		GenericItem tester = new GenericItem("content");
		tester.setContent("new content");
	}

}
